---
layout: post
title: diffoscope 224 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `224`. This version includes the following changes:

```
[ Mattia Rizzolo ]
* Fix rlib test failure with LLVM 15. Thanks to Gianfranco Costamagna
  (locutusofborg) for the patch.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 280 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `280`. This version includes the following changes:

```
[ Chris Lamb ]
* Drop Depends on deprecated python3-pkg-resources. (Closes: #1083362)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

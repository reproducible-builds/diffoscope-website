---
layout: post
title: diffoscope 251 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `251`. This version includes the following changes:

```
* If the equivalent of `file -i` returns text/plain, fallback to comparing
  this file as a text file. This especially helps when file(1) miscategorises
  text files as some esoteric type. (Closes: Debian:#1053668)
* Update copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

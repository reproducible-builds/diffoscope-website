---
layout: post
title: diffoscope 276 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `276`. This version includes the following changes:

```
[ Chris Lamb ]
* Also catch RuntimeError when importing PyPDF so that PyPDF or, crucially,
  its transitive dependencies do not cause diffoscope to traceback at runtime
  and build time. (Closes: #1078944, reproducible-builds/diffoscope#389)
* Factor out a method for stripping ANSI escapes.
* Strip ANSI escapes from the output of Procyon. Thanks, Aman Sharma!
* Update copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

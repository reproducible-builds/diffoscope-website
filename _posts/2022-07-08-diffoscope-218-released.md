---
layout: post
title: diffoscope 218 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `218`. This version includes the following changes:

```
* Improve output of Markdown and reStructuredText to use code blocks with
  syntax highlighting. (Closes: reproducible-builds/diffoscope#306)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

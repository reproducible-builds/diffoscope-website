---
layout: post
title: diffoscope 289 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `289`. This version includes the following changes:

```
[ Chris Lamb ]
* Catch CalledProcessError when calling html2text.
* Update copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 231 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `231`. This version includes the following changes:

```
* Improve "[X] may produce better output" messages. Based on a patch by
  Helmut Grohne. (Closes: #1026982)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 110 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `110`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't look for adjacent -dbgsym package files automatically anymore to
  align better with default user expectations. The existing behaviour can be
  re-enabled by specifying the new `--use-dbgsym` flag. (Closes: #920701)
* Don't crash with if we were unable to successfully extract a
  "guestfs"-based file. (Closes: #901982)
* Avoid clumsy profiling title length calculations by moving to Markdown
  syntax, from reStructuredText.
* Drop printing out dpkg-query output when running tests: it's rather noisy
  and has not helped yet in reproducing an error.
* Re-enable gnumeric as a Build-Depends.
* debian/rules: Use str.format over "+" for string concatenation.

[ Jelle van der Waa ]
* comparators.wasm: Remove an unused "re" import. (MR: !18)

[ Mattia Rizzolo ]
* comparators/elf: Apply flake8.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 285 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `285`. This version includes the following changes:

```
[ Chris Lamb ]
* Validate --css command-line argument. Thanks to Daniel Schmidt @ SRLabs for
  the report. (Closes: #396)
* Prevent XML entity expansion attacks through vulnerable versions of
  pyexpat. Thanks to Florian Wilkens @ SRLabs for the report. (Closes: #397)
* Print a warning if we have disabled XML comparisons due to a potentially
  vulnerable version of pyexpat.
* Remove (unused) logging facility from a few comparators.
* Update copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

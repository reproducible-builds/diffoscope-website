---
layout: post
title: diffoscope 158 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `158`. This version includes the following changes:

```
* Improve PGP support:
  - Support extracting of files within PGP signed data.
    (Closes: reproducible-builds/diffoscope#214)
  - pgpdump(1) can successfully parse some unrelated, non-PGP binary files,
    so check that the parsed output contains something remotely sensible
    before identifying it as a PGP file.
* Don't use Python's repr(...)-style output in "Calling external command"
  logging output.
* Correct a typo of "output" in an internal comment.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 210 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `210`. This version includes the following changes:

```
[ Mattia Rizzolo ]
* Make sure that PATH is properly mangled for all diffoscope actions, not
  just when running comparators.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

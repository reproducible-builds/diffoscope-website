---
layout: post
title: diffoscope 253 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `253`. This version includes the following changes:

```
* Improve DOS/MBR extraction by adding support for 7z.
  (Closes: reproducible-builds/diffoscope#333)
* Process objdump symbol comment filter inputs as the Python "bytes" type
  (and not str). (Closes: reproducible-builds/diffoscope#358)
* Add a missing RequiredToolNotFound import.
* Update copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

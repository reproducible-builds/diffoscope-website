---
layout: post
title: diffoscope 239 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `239`. This version includes the following changes:

```
[ Chris Lamb ]
* Fix compatibility with pypdf 3.x, and correctly restore test data.
  (Closes: reproducible-builds/diffoscope#335)
* Rework PDF annotations processing into a separate method.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 274 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `274`. This version includes the following changes:

```
[ Chris Lamb ]
* Add support for IO::Compress::Zip >= 2.212. (Closes: #1078050)
* Don't include debug output when calling dumppdf(1).
* Append output from dumppdf(1) in more cases.
  (Closes: reproducible-builds/diffoscope#387)
* Update copyright years.

[ Mattia Rizzolo ]
* Update the available architectures for test dependencies.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 101 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `101`. This version includes the following changes:

```
* Fix testsuite under LLVM >= 7.0. (Closes: #908074)
* Substvar generation:
  - Ensure Debian substvar generation is deterministic regardless of
    installed packages. (Closes: #908072)
  - Clarify distinction between tools (eg. gpg) and packages (eg. gnupg) when
    generating Debian substvars.
  - Revert updating generated debian/tests/control made in diffoscope 100.
* Disable binwalk's own user configuration for predictable results and to
  ensure it does not create unnecessary directories. (Closes: #903444)
* Ensure we return "bytes" objects from Command.filter to avoid a
  "TypeError: Unicode-objects must be encoded before hashing" traceback.
* Don't print GPG output in diffoscope.changes.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

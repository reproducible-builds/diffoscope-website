---
layout: post
title: diffoscope 125 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `125`. This version includes the following changes:

```
* The test_libmix_differences ELF test requires xxd. (Closes: #940645)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

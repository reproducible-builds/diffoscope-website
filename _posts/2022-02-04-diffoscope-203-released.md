---
layout: post
title: diffoscope 203 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `203`. This version includes the following changes:

```
[ Chris Lamb ]
* Improve documentation for --timeout due to a few misconceptions.
  Add an allowed-to-fail test regarding a regression in directory handling.
* Tidy control flow in Difference._reverse_self a little.

[ Alyssa Ross ]
* Fix diffing CBFS names that contain spaces.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

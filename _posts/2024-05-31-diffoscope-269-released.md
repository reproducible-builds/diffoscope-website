---
layout: post
title: diffoscope 269 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `269`. This version includes the following changes:

```
[ Chris Lamb ]
* Allow Debian testing continuous integration builds to fail right now.

[ Sergei Trofimovich ]
* Amend 7zip version test for older versions that include the "[64]" string.
  (Closes: reproducible-builds/diffoscope#376)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

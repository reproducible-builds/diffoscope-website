---
layout: post
title: diffoscope 260 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `260`. This version includes the following changes:

```
[ Chris Lamb ]
* Actually test 7z support in the test_7z set of tests, not the lz4
  functionality. (Closes: reproducible-builds/diffoscope#359)
* In addition, correctly check for the 7z binary being available
  (and not lz4) when testing 7z.
* Prevent a traceback when comparing a contentful .pyc file with an
  empty one. (Re: Debian:#1064973)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

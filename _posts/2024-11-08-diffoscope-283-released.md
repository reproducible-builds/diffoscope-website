---
layout: post
title: diffoscope 283 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `283`. This version includes the following changes:

```
[ Martin Abente Lahaye ]
* Fix crash when objdump is missing when checking .EFI files.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

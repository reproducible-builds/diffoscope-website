---
layout: post
title: diffoscope 279 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `279`. This version includes the following changes:

```
[ Chris Lamb ]
* Drop removal of calculated basename from readelf output.
  (Closes: reproducible-builds/diffoscope#394)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 130 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `130`. This version includes the following changes:

```
[ Chris Lamb ]
* debian/tests/basic-command-line:
  Move from deprecated ADTTMP to AUTOPKGTEST_TMP.
* Correct the matching of R .rds files by also detecting newer versions
  of this file format.
* Drop unused BASE_DIR global in the tests.
* Try and ensure that new test data files are generated dynamically, ie.
  at least no new ones are added without "good" reasons.
* Truncate the tcpdump expected diff to 8KB (from ~600KB).
* Refresh OCaml test fixtures to support OCaml >= 4.08.1.  Closes: #944709
* Correct reference to the ".rdx" extension in a comment.
* Update XML test for Python 3.8+.
* Don't use line-base dbuffering when communucating with subprocesses
  in binary mode. (Closes: reproducible-builds/diffoscope#75)

[ Jelle van der Waa ]
* Add support for comparing .zst files are created by zstd.
  (Closes: reproducible-builds/diffoscope!34)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

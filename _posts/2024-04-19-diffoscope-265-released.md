---
layout: post
title: diffoscope 265 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `265`. This version includes the following changes:

```
[ Chris Lamb ]
* Ensure that tests with ">=" version constraints actually print the
  corresponding tool name. (Closes: reproducible-builds/diffoscope#370)
* Prevent odt2txt tests from always being skipped due to an impossibly new
  version requirement. (Closes: reproducible-builds/diffoscope#369)
* Avoid nested parens-in-parens when printing "skipping…" messages
  in the testsuite.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 34 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `34`. This version includes the following changes:

```
* Fix path to executable in README. Thanks Niko Tyni for the patch.
* Encode file path when filtering it from readelf output. This fixes
  diffoscope for gnome-clocks.  (Closes: #798398)
* Include ELF test files in the source tarball.
* Fix required tool for zip tests.
* Fix expected `unsquashfs -s` output for squashfs-tools 4.3.
* Parse md5sums in .deb even if they are identical.
* Log ignore files count when comparing data.tar in .deb.
* Minor code improvemnts.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 275 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `275`. This version includes the following changes:

```
[ Chris Lamb ]
* Update the test_zip.py text fixtures and definitions to support new changes
  to IO::Compress. (Closes: #1078050)
* Do not call marshal.loads(...) of precompiled Python bytecode as it is
  inherently unsafe. Replace, at least for now, with a brief summary of the
  code section of .pyc files. (Re: reproducible-builds/diffoscope#371)
* Don't bother to check the Python version number in test_python.py: the
  fixture for this test is deterministic/fixed.
* Update copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

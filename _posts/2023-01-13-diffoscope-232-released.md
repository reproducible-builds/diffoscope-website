---
layout: post
title: diffoscope 232 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `232`. This version includes the following changes:

```
[ Chris Lamb ]
* Allow ICC tests to (temporarily) fail.
* Update debian/tests/control after the addition of PyPDF 3 support.

[ FC Stegerman ]
* Update regular expression for Android .APK files.

[ Sam James ]
* Support PyPDF version 3.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

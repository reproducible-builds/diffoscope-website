---
layout: post
title: diffoscope 208 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `208`. This version includes the following changes:

```
[ Brent Spillner ]
* Add graceful handling for UNIX sockets and named pipes.
  (Closes: reproducible-builds/diffoscope#292)
* Remove a superfluous log message and reformatt comment lines.

[ Chris Lamb ]
* Reformat various files to satisfy current version of Black.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 288 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `288`. This version includes the following changes:

```
[ Chris Lamb ]
* Add 'asar' to DIFFOSCOPE_FAIL_TESTS_ON_MISSING_TOOLS. (Closes: #1095057)
* Update minimal 'black' version.
* Update copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

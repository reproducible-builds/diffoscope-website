---
layout: post
title: diffoscope 206 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `206`. This version includes the following changes:

```
* Also allow "Unicode text, UTF-8 text" as well as "UTF-8 Unicode text" to
  match for .buildinfo files too.
* Add a test for recent file(1) issue regarding .changes files.
  (Re: reproducible-builds/diffoscope#291)
* Drop "_PATH" suffix from some module-level globals that are not paths.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

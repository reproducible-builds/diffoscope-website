---
layout: post
title: diffoscope 204 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `204`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't run the binwalk comparator tests as root (or fakeroot) as the
  latest version of binwalk has some security protection against doing
  precisely this.
* If we fail to scan a file using binwalk, return 'False' from
  BinwalkFile.recognizes rather than raise a traceback.
* If we fail to import the Python "binwalk" module, don't accidentally report
  that we are missing the "rpm" module instead.

[ Mattia Rizzolo ]
* Use dependencies to ensure that "diffoscope" and "diffoscope-minimal"
  packages always have the precise same version.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 168 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `168`. This version includes the following changes:

```
* Don't call difflib.Differ.compare with very large inputs; it is at least
  O(n^2) and makes diffoscope appear to hang.
  (Closes: reproducible-builds/diffoscope#240)
* Don't use "Inheriting PATH of X" in debug log message; use "PATH is X".
* Correct the capitalisation of jQuery.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

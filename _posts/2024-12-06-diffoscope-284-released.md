---
layout: post
title: diffoscope 284 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `284`. This version includes the following changes:

```
[ Chris Lamb ]
* Simplify tests_quines.py::test_{differences,differences_deb} to use
  assert_diff and not mangle the expected test output.
* Update some tests to support file(1) version 5.46.
  (Closes: reproducible-builds/diffoscope#395)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

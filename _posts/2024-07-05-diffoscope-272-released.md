---
layout: post
title: diffoscope 272 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `272`. This version includes the following changes:

```
[ Chris Lamb]
* Move away from using DSA OpenSSH keys in tests; support has been removed
  in OpenSSH 9.8p1. (Closes: reproducible-builds/diffoscope#382)
* Move to assert_diff helper in test_openssh_pub_key.py
* Update copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

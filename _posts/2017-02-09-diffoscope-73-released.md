---
layout: post
title: diffoscope 73 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `73`. This version includes the following changes:

```
* debian/tests/pytest: Remove spurious clipboard contents.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

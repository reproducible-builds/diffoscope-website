---
layout: post
title: diffoscope 241 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `241`. This version includes the following changes:

```
[ Chris Lamb ]
* Add a missing 'raise' statement dropped in 2d95ae41e. Thanks, Mattia!

[ Mattia Rizzolo ]
* document sending out an email upon release
```

You find out more by [visiting the project homepage](https://diffoscope.org).

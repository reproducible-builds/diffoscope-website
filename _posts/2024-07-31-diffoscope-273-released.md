---
layout: post
title: diffoscope 273 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `273`. This version includes the following changes:

```
[ Chris Lamb ]
* Factor out version detection in test_jpeg_image. (Re:
  reproducible-builds/diffoscope#384)
* Ensure that 'convert' is from Imagemagick 6.x; we will need to update a
  few things with IM7. (Closes: reproducible-builds/diffoscope#384)
* Correct import of identify_version after refactoring change in 037bdcbb0.

[ Mattia Rizzolo ]
* tests:
  + Add OpenSSH key test with a ed25519 key.
  + Skip the OpenSSH test with DSA key if openssh is >> 9.7
  + Support ffmpeg >= 7 that adds some extra context to the diff
* Do not ignore testing in gitlab-ci.
* debian:
  + Temporarily remove aapt, androguard and dexdump from the build/test
    dependencies as they are not available in testin/trixie.  Closes: #1070416
  + Bump Standards-Version to 4.7.0, no changes needed.
  + Adjust options to make sure not to pack the python s-dist directory
    into the debian source package.
  + Adjust the lintian overrides.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

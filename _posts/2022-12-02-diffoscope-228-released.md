---
layout: post
title: diffoscope 228 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `228`. This version includes the following changes:

```
[ FC Stegerman ]
* As an optimisation, don't run apktool if no differences are detected before
  the signing block. (Closes: reproducible-builds/diffoscope!105)

[ Chris Lamb ]
* Support both the python3-progressbar and python3-progressbar2 Debian
  packages, two modules providing the "progressbar" Python module.
  (Closes: reproducible-builds/diffoscope#323)
* Ensure we recommend apksigcopier. (Re: reproducible-builds/diffoscope!105)
* Make the code clearer around generating the Debian substvars and tidy
  generation of os_list.
* Update copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

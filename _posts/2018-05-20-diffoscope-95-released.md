---
layout: post
title: diffoscope 95 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `95`. This version includes the following changes:

```
[ Mattia Rizzolo ]
* tests:
  + test_binary: Don't capture unused output from subprocess.
  + test_git: Fix test failure on FreeBSD.  Closes: #872826
    Thanks to Ximin Luo <infinity0@debian.org> for the initial patch
* Fix handling of filesnames with non-unicode chars.  Closes: #898022
* diff: Use bytes as much as possible, to prevent possible encoding issues.
* d/control: Make the dependency on python3-distutils an alternative on the
  previous versions of libpython3.*-stdlib that used to ship it.
  Closes: #898683
* d/watch: Update URL to the new archive.

[ Chris Lamb ]
* The Git repository has been migrated to salsa, update all the references.
* Drop extra whitespace in supported file format output.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 258 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `258`. This version includes the following changes:

```
[ Chris Lamb ]
* Use the 7zip package (over p7zip-full) after package transition.
  (Closes: #1063559)
* Update debian/tests/control.

[ Vagrant Cascadian ]
* Fix a typo in the package name field (!) within debian/changelog.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

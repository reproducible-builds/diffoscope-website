---
layout: post
title: diffoscope 134 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `134`. This version includes the following changes:

```
[ Chris Lamb ]
* Ensure that autopkgtests are run with our pyproject.toml present for the
  correct "black" source code formatter settings. (Closes: #945993)
* Tidy some unnecessary boolean logic in the ISO9660 tests.
* Rename the "text_option_with_stdiout" to "text_option_with_stdout".
* Include the libarchive file listing for ISO images to ensure that
  timestamps (not just dates) are visible in any difference.
  (Closes: reproducible-builds/diffoscope#81)

[ Eli Schwartz ]
* Fix an exception in the progressbar handler.

[ Vagrant Cascadian ]
* Add an external tool reference for zstd on GNU Guix.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

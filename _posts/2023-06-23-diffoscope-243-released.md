---
layout: post
title: diffoscope 243 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `243`. This version includes the following changes:

```
[ Chris Lamb ]
* Drop Jenkins build reference in README.rst.

[ Ed Maste ]
* Update FreeBSD package names

[ Mattia Rizzolo ]
* Improve the documentation on to produce that binary blob that in the arsc
  comparator.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

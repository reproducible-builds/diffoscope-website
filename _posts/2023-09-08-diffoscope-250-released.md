---
layout: post
title: diffoscope 250 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `250`. This version includes the following changes:

```
[ Chris Lamb ]
* Fix compatibility with file 5.45. (Closes: reproducible-builds/diffoscope#351)

[ Vagrant Cascadian ]
* Add external tool references for GNU Guix (for html2text and ttx).
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 216 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `216`. This version includes the following changes:

```
* Print profile output if we were called with --profile and we receive a
  TERM signal.
* Emit a warning if/when we are handling a TERM signal.
* Clarify in the code in what situations the main "finally" block gets
  called, especially in relation to handling TERM signals.
* Clarify and tidy some unconditional control flow in diffoscope.profiling.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

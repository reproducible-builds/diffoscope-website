---
layout: post
title: diffoscope 42 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `42`. This version includes the following changes:

```
* Add missing Depends on python3-pkg-resources.
* Add another autopkgtest to test command line without recommends.
* Make comparison of zip archives with utf-8 file names more robust.
  (Closes: #805418)
* Stop crashing on misencoded readelf output. (Closes: #804061)
* Ignore encoding errors in diff output.
* Add binutils-multiarch to Build-Depends so the test suite can pass
  on architectures other than amd64.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

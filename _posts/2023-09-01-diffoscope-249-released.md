---
layout: post
title: diffoscope 249 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `249`. This version includes the following changes:

```
[ FC Stegerman ]
* Add specialize_as() method, and use it to speed up .smali comparison in
  APKs. (Closes: reproducible-builds/diffoscope!108)

[ Chris Lamb ]
* Add documentation for the new specialize_as, and expand the documentation
  of `specialize` too. (Re: reproducible-builds/diffoscope!108)
* Update copyright years.

[ Felix Yan ]
* Correct typos in diffoscope/presenters/utils.py.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

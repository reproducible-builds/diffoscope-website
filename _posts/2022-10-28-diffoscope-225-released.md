---
layout: post
title: diffoscope 225 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `225`. This version includes the following changes:

```
[ Chris Lamb ]
* Add support for detecting ordering-only differences in XML files.
  (Closes: #1022146)
* Fix an issue with detecting ordering differences. (Closes: #1022145)
* Add support for ttx(1) from fonttools.
  (Re: reproducible-builds/diffoscope#315)
* Test improvements:
  - Tidy up the JSON tests and use assert_diff over get_data and manual
    assert in XML tests.
  - Rename order1.diff to json_expected_ordering_diff for consistency.
  - Temporarily allow the stable-po pipeline to fail in the CI.
* Use consistently capitalised "Ordering" everywhere we use the word in
  diffoscope's output.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

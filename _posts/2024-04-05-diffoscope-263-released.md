---
layout: post
title: diffoscope 263 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `263`. This version includes the following changes:

```
[ Chris Lamb ]
* Add support for the zipdetails(1) tool included in the Perl distribution.
  Thanks to Larry Doolittle et al. for the pointer to this tool.
* Don't use parenthesis within test "skipping…" messages; PyTest adds its own
  parenthesis, so we were ending up with double nested parens.
* Fix the .epub tests after supporting zipdetails(1).
* Update copyright years and debian/tests/control.

[ FC (Fay) Stegerman ]
* Fix MozillaZipContainer's monkeypatch after Python's zipfile module changed
  to detect potentially insecure overlapping entries within .zip files.
  (Closes: reproducible-builds/diffoscope#362)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

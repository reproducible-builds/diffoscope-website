---
layout: post
title: diffoscope 50 released
author: Holger Levsen <holger@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `50`. This version includes the following changes:

```
[ Jérémy Bobbio ]
* Remove myself from uploaders

[ Reiner Herrmann ]
* Show line ordering variation in deb md5sums
* Reset mtimes of test directories and ignore ctimes (Closes: #815171)
* Add comparator for Postscript files
```

You find out more by [visiting the project homepage](https://diffoscope.org).

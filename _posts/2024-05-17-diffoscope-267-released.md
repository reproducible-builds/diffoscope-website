---
layout: post
title: diffoscope 267 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `267`. This version includes the following changes:

```
[ Chris Lamb ]
* Include "xz --verbose --verbose" (ie. double --verbose) output, not just
  the single --verbose. (Closes: #1069329)
* Only include "xz --list" output if the xz has no other differences.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

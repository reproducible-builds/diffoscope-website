---
layout: post
title: diffoscope 66 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `66`. This version includes the following changes:

```
[ Chris Lamb ]
* Update dex_expected_diffs and test requirement to ensure test compatibility
  with enjarify >= 1.0.3. (Closes: #849142)
* Print the detected version in @skip_unless_tool_is_at_least test utility.

[ Maria Glukhova ]
* Add detection of order-only difference in plain text format.
  (Closes: #848049)

[ anthraxx ]
* Add OpenSSH Arch package to Recommends.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

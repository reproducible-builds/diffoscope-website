---
layout: post
title: diffoscope 281 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `281`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't try and test with systemd-ukify within Debian stable.

[ Jelle van der Waa ]
* Add support for UKI files.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 45 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `45`. This version includes the following changes:

```
[ Rainer Müller ]
* Fix typo in Mach-O comparator.

[ Jérémy Bobbio ]
* Ensure that we always get path names from libarchive as str.
  (Closes: #808541)
* Read ext4 reference diff using UTF-8 to ensure that the test suite
  works when run under various locales.
* Replace file path by a fixed placeholder in readelf and objdump output.
  This will work better when comparing ELF files given on the command line.
* Read lines using an iterator instead of loading a full list in memory.
  This greatly help diffoscope when running on large binaries. Thanks Mike
  Hommey for the report and good test case. (Closes: #808120)
* Properly report lines we can't parse in squashfs listing.
* Correctly parse squashfs device entries with device minors larger than 3
  digits. Thanks sajolida for providing the samples.
* Use libarchive to list files in tar and cpio archives. The output of cpio
  and tar is too hard to control properly and tend to vary over time.
  (Closes: #808809)
* Ensure files extracted from a squashfs image are deleted after being
  compared instead being done examining the image.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 197 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `197`. This version includes the following changes:

```
[ Chris Lamb ]
* Drop unnecessary has_same_content_as logging calls.

[ Mattia Rizzolo ]
* Ignore the new "binary-with-bad-dynamic-table" Lintian tag.
* Support pgpdump 0.34 in the tests. Thanks to Michael Weiss
  <dev.primeos@gmail.com> for reporting and testing the fix.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 194 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `194`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't traceback when comparing nested directories with non-directories.
  (Closes: reproducible-builds/diffoscope#288)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

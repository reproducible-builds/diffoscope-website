---
layout: post
title: diffoscope 11 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `11`. This version includes the following changes:

```
* Use latin-1 to decode showttf output.
* Normalize locale environment variables on startup and subsequently use
  UTF-8 to decode output of most external tools. (Closes: #780863)
* Error out when encoding is not Unicode-compatible for text output.
  (Closes: #778641)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

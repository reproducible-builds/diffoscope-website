---
layout: post
title: diffoscope 233 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `233`. This version includes the following changes:

```
[ FC Stegerman ]
* Split packaging metadata into an extras_require.json file instead of using
  the pep517 and the pip modules directly. This was causing build failures if
  not using a virtualenv and/or building without internet access.
  (Closes: #1029066, reproducible-builds/diffoscope#325)

[ Vagrant Cascadian ]
* Add an external tool reference for GNU Guix (lzip).
* Drop an external tool reference for GNU Guix (pedump).

[ Chris Lamb ]
* Split inline Python code in shell script to generate test dependencies to a
  separate Python script.
* No need for "from __future__ import print_function" import in setup.py
  anymore.
* Comment and tidy the new extras_require.json handling.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

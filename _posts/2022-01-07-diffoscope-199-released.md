---
layout: post
title: diffoscope 199 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `199`. This version includes the following changes:

```
[ Chris Lamb ]
* Support both variants of "odt2txt", including the one provided by unoconv.
  (Closes: reproducible-builds/diffoscope#298)

[ Jelle van der Waa ]
* Add external tool reference on Arch Linux for xb-tool.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

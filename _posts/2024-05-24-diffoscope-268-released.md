---
layout: post
title: diffoscope 268 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `268`. This version includes the following changes:

```
[ Chris Lamb ]
* Drop apktool from Build-Depends; we can still test our APK code
  via autopkgtests. (Closes: #1071410)
* Fix tests for 7zip version 24.05.
* Add a versioned dependency for at least version 5.4.5 for the xz
  tests; they fail under (at least xz 5.2.8).
  (Closes: reproducible-builds/diffoscope#374)

[ Vagrant Cascadian ]
* Relax Chris' versioned xz test dependency (5.4.5) to also allow
  version 5.4.1.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 220 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `220`. This version includes the following changes:

```
* Support Haskell 9.x series files and update the test files to match. Thanks
  to Scott Talbert for the relevant info about the new format.
  (Closes: reproducible-builds/diffoscope#309)
* Fix a regression introduced in diffoscope version 207 where diffoscope
  would crash if one directory contained a directory that wasn't in the
  other. Thanks to Alderico Gallo for the report and the testcase.
  (Closes: reproducible-builds/diffoscope#310)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

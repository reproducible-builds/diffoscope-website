---
layout: post
title: diffoscope 74 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `74`. This version includes the following changes:

```
* Add missing Recommends for comparators. This was a regression introduced in
  version 71 due to lazily-importing them; they were then not available when
  we called "--list-tools=debian" during package build. (Closes: #854655)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 277 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `277`. This version includes the following changes:

```
[ Sergei Trofimovich ]
* Don't crash when attempting to hashing symlinks with targets that point to
  a directory.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 90 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `90`. This version includes the following changes:

```
[ Juliana Oliveira ]
* tests:
  + comparators/test_rlib: Fix tests for llvm >= 5.0.  Closes: #877727
  + Skip some tests if 'xz' is not present.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 124 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `124`. This version includes the following changes:

```
[ Chris Lamb ]
* Also conditionally skip the identification and "no differences" tests as we
  require the Ocaml compiler to be present when building the test files
  themselves. (Closes: #940471)

[ Mattia Rizzolo ]
* Permit all sorts of version suffixes when checking the Debian package
  version matches setup.py, not just for backports. (Closes: #939387)

[ Vagrant Cascadian ]
* Add external tools on GNU Guix for odt2txt, sng and pgpdump.

[ Marc Herbert ]
* Remove StaticLibFile in the ELF comparator -- ArFile.compare_details() is
  superior. (Closes: reproducible-builds/diffoscope#64)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 237 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `237`. This version includes the following changes:

```
* autopkgtest: only install aapt and dexdump on architectures where they are
  available. (Closes: #1031297)
* compartors/pdf:
  + Drop backward compatibility assignment.
  + Fix flake warnings, potentially reinstating PyPDF 1.x support (untested).
```

You find out more by [visiting the project homepage](https://diffoscope.org).

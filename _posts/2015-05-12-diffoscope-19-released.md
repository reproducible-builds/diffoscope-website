---
layout: post
title: diffoscope 19 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `19`. This version includes the following changes:

```
[ Reiner Herrmann ]
* Fix info files comparator by specifying the correct parameter name
  for the text comparator. (Closes: #784891)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 182 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `182`. This version includes the following changes:

```
[ Chris Lamb ]
* Also ignore, for example, spurious "fwGCC: (Debian ... )" lines in output
  from strings(1).
* Only use "java -jar /path/to/apksigner.jar" if we have a .jar as newer
  versions of apksigner use a shell wrapper script which will obviously be
  rejected by the JVM. Also mention in the diff if apksigner is missing.
* Pass "-f" to apktool to avoid creating a strangely-named subdirectory and
  to simplify code.
* If we specify a suffix for temporary file or directory, ensure it starts
  with a "_" to make the generated filenames more human-readable.
* Drop an unused File import.
* Update the minimum version of the Black source code formatter.

[ Santiago Torres Arias ]
* Support parsing the return value of squashfs versions which discriminate
  between fatal and non-fatal errors.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

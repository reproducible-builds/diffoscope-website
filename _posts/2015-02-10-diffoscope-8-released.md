---
layout: post
title: diffoscope 8 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `8`. This version includes the following changes:

```
[ Peter De Wachter ]
* Use `diff -a` so debbindiff doesn't abort on binary files.

[ Jérémy Bobbio ]
* Verify that version in debian/changelog matches debbindiff version.
* Update debian/copyright.
* Recognize text/plain files.

[ Reiner Herrmann ]
* Increased line context of diff to further lower running time.
* Use text comparator for xml/xhtml files.
* Use text comparator for postscript files.
* Add support for cpio archives.
* Add support for rpm packages.
* Fix a typo in debbindiff name in source headers.

[ Chris Lamb ]
* Improve code quality in several places.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

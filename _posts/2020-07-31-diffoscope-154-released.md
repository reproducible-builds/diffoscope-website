---
layout: post
title: diffoscope 154 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `154`. This version includes the following changes:

```
[ Chris Lamb ]

* Add support for F2FS filesystems.
  (Closes: reproducible-builds/diffoscope#207)
* Allow "--profile" as a synonym for "--profile=-".
* Add an add_comment helper method so don't mess with our _comments list
  directly.
* Add missing bullet point in a previous changelog entry.
* Use "human-readable" over unhyphenated version.
* Add a bit more debugging around launching guestfs.
* Profile the launch of guestfs filesystems.
* Correct adding a comment when we cannot extract a filesystem due to missing
  guestfs module.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

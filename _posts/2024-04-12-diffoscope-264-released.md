---
layout: post
title: diffoscope 264 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `264`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't crash on invalid zipfiles, even if we encounter 'badness'
  halfway through the file. (Re: #1068705)

[ FC (Fay) Stegerman ]
* Fix a crash when there are (invalid) duplicate entries in .zip files.
  (Closes: #1068705)
* Add note when there are duplicate entries in ZIP files.
  (Closes: reproducible-builds/diffoscope!140)

[ Vagrant Cascadian ]
* Add an external tool reference for GNU Guix for zipdetails.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

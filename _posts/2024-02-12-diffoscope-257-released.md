---
layout: post
title: diffoscope 257 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `257`. This version includes the following changes:

```
[ James Addison ]
* Parse the header and hunksize of diffs strictly before parsing the context
  below. (Closes: reproducible-builds/diffoscope#363)
* Reformat code to comply with the latest version of Black (24.1.1).

[ Chris Lamb ]
* Expand the previous changelog entry to include the CVE number that was
  subsequently assigned.
* Bump the miniumum Black requirement to run the "Black clean" test and make
  test_zip.py Black clean.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

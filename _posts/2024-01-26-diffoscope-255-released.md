---
layout: post
title: diffoscope 255 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `255`. This version includes the following changes:

```
[ Vekhir ]
* Add/fix compatibility for Python progressbar 2.5 & 3.0 etc.

[ Chris Lamb ]
* Update copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 171 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `171`. This version includes the following changes:

```
[ Mattia Rizzolo ]
* Do not list as a "skipping reason" tools that do exist.
* Drop the "compose" tool from the list of required tools for these tests,
  since it doesn't seem to be required.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

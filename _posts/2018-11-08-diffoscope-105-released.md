---
layout: post
title: diffoscope 105 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `105`. This version includes the following changes:

```
[ Chris Lamb ]
* tests:
  + Prevent test failures when running under stretch-backports by checking
    the ocaml version number.  Closes: #911846

[ Mattia Rizzolo ]
* debian: Reinstate apktool Build-Depends and Test-Depends.
* Fix some flake8 and deprecation warnings.

[ Daniel Shahaf ]
* comparators/pcap:
  + Fix recognition with the upcoming file(1) 5.35.  Closes: #912756
    Thanks to Christoph Biedl for the heads-up in advance.

[ Will Thompson ]
* Add a new command line flag --list-missing-tools.  MR: !14
```

You find out more by [visiting the project homepage](https://diffoscope.org).

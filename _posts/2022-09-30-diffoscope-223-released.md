---
layout: post
title: diffoscope 223 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `223`. This version includes the following changes:

```
[ Chris Lamb ]
* The cbfstools utility is now provided in Debian via the coreboot-utils
  Debian package, so we can enable that functionality within Debian.
  (Closes: #1020630)

[ Mattia Rizzolo ]
* Also include coreboot-utils in Build-Depends and Test-Depends so it is
  available for the tests.

[ Jelle van der Waa ]
* Add support for file 5.43.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

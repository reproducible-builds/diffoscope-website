---
layout: post
title: diffoscope 174 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `174`. This version includes the following changes:

```
[ Chris Lamb ]
* Check that we are parsing an actual Debian .buildinfo file, not just
  a file with that extension.
  (Closes: #987994, reproducible-builds/diffoscope#254)
* Support signed .buildinfo files again -- file(1) reports them as
  "PGP signed message".

[ Mattia Rizzolo ]
* Make the testsuite pass with file(1) version 5.40.
* Embed some short test fixtures in the test code itself.
* Fix recognition of compressed .xz files with file(1) 5.40.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 126 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `126`. This version includes the following changes:

```
[ Chris Lamb ]
* Track and report on missing Python modules.
  (Closes: reproducible-builds/diffoscope#72)
* Drop some unnecessary control flow.
* Drop explicit inheriting from 'object' class; unnecessary in Python 3.
* Bump Standards-Version to 4.4.1.

[ Mattia Rizzolo ]
* Exit with 2 instead of 1 in case of no disk space.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 114 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `114`. This version includes the following changes:

```
[ Chris Lamb ]
* Add support for GnuPG "keybox" files. Thanks to Daniel Kahn Gillmor for the
  suggestion. (Closes: #871244, reproducible-builds/diffoscope#23)
* Always warn if tlsh module is not available (not just if a specific
  fuzziness threshold is specified) to match the epilog of the --help output.
  This prevents missing support for file rename detection.
  (Closes: #888237, reproducible-builds/diffoscope#29)
* Treat missing tools on Debian autopkgtests as individual test failures by
  checking whether a new DIFFOSCOPE_TESTS_FAIL_ON_MISSING_TOOLS environment
  variable is exported. (Closes: #905885, reproducible-builds/diffoscope#35)
* Require that "-" is explicitly specified to read a single diff from
  standard input to avoid non-intuitive behaviour when diffoscope is called
  without any arguments. (Closes: reproducible-builds/diffoscope#54)
* Make --use-dbgsym a ternary operator to make it easier to totally disable.
  Thanks to Mattia Rizzolo for the suggestion.
* Consolidate on "e" as the aliased exception name.

[ Milena Boselli Rosa ]
* Various fixes to the HTML markup to prevent validation warnings/errors:
  - Prevent empty values for the "name" attribute name on HTML anchor tags,
    and add an "id" to its parent "div" container.
  - Fix "table column x established by element 'col' has no cells beginning
    in it" warnings.
  - Fix "Text run is not in Unicode Normalization Form C".
  - Remove the "type" HTML attribute from <style> elements.

[ Vibhu ]
* Fail more gracefully when running out of diskspace. (Closes: #874582)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

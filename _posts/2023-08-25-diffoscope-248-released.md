---
layout: post
title: diffoscope 248 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `248`. This version includes the following changes:

```
[ Greg Chabala ]
* Merge Docker "RUN" commands into single layer.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

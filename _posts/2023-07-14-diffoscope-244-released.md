---
layout: post
title: diffoscope 244 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `244`. This version includes the following changes:

```
[ Chris Lamb ]
* Address compatibility with python-libarchive-c version 5.
  (Closes: reproducible-builds/diffoscope#344)
* Testsuite changes:
  - Mark that test_dex::test_javap_14_differences requires procyon.
  - Fix "test skipped" textual reason generation in the case of a required
    version being outside of the required range.
  - Temporarily mark some Android-related as XFAIL due to Debian bugs
    #1040941 and #1040916.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

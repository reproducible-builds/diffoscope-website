---
layout: post
title: diffoscope 93 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `93`. This version includes the following changes:

```
* Only append the file formats if --help is passed, otherwise we spam
  #debian-reproducible-changes et al. (re. #893443)
* doc/Makefile: Reinstate accidental version parsing change; my sed call
  was over-eager.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

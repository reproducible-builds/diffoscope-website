---
layout: post
title: diffoscope 242 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `242`. This version includes the following changes:

```
* If the binwalk Python module is not available, ensure the user knows they
  may be missing more differences in, for example, concatenated .cpio
  archives.
* Factor out routine to generate a human-readable comments when
  Python modules are missing.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

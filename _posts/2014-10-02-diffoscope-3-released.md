---
layout: post
title: diffoscope 3 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `3`. This version includes the following changes:

```
* Initial Debian release. (Closes: #763328)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

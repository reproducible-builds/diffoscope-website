---
layout: post
title: diffoscope 238 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `238`. This version includes the following changes:

```
* autopkgtest: fix tool name in the skippable list.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 259 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `259`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't error-out with a traceback if we encounter "struct.unpack"-related
  errors when parsing .pyc files. (Closes: #1064973)
* Fix compatibility with PyTest 8.0. (Closes: reproducible-builds/diffoscope#365)
* Don't try and compare rdb_expected_diff on non-GNU systems as %p formatting
  can vary. (Re: reproducible-builds/diffoscope#364)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 282 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `282`. This version includes the following changes:

```
[ Chris Lamb ]
* Ignore errors when listing .ar archives. (Closes: #1085257)
* Update copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

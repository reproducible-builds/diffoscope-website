---
layout: post
title: diffoscope 201 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `201`. This version includes the following changes:

```
[ Chris Lamb ]
* If the debian.deb822 module raises any exception on import, re-raise it as
  an ImportError instead. This should fix diffoscope on some Fedora systems.
  Thanks to Mattia Rizzolo for suggesting this particular solution.
  (Closes: reproducible-builds/diffoscope#300)

[ Zbigniew Jędrzejewski-Szmek ]
* Fix json detection with file-5.41-3.fc36.x86_64.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

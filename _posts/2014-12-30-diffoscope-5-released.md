---
layout: post
title: diffoscope 5 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `5`. This version includes the following changes:

```
* Properly encode strings before writing them to tempfiles.
  (Closes: #764254)
* Fallback on binary comparison when text encoding is unknown.
* Always use given source path in binary_fallback.
* Add support for .udeb.
* Add support for ttf and otf files.
* Add support for png files. (Closes: #773573)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

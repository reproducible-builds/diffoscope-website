---
layout: post
title: diffoscope 198 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `198`. This version includes the following changes:

```
[ Chris Lamb ]
* Support showing "Ordering differences only" within .dsc field values.
  (Closes: #1002002, reproducible-builds/diffoscope#297)
* Support OCaml versions 4.11, 4.12 and 4.13. (Closes: #1002678)
* Add support for XMLb files. (Closes: reproducible-builds/diffoscope#295)
* Also add, for example, /usr/lib/x86_64-linux-gnu to our internal PATH.

[ Mattia Rizzolo ]
* Also recognize "GnuCash file" files as XML.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

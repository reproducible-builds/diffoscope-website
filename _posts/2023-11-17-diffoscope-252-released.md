---
layout: post
title: diffoscope 252 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `252`. This version includes the following changes:

```
* As UI/UX improvement, try and avoid printing an extended traceback if
  diffoscope runs out of memory. This may not always be possible to detect.
* Mark diffoscope as stable in setup.py (for PyPI.org). Whatever diffoscope
  is, at least, not "alpha" anymore.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 236 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `236`. This version includes the following changes:

```
[ FC Stegerman ]
* Update code to match latest version of Black. (Closes: #1031433)

[ Chris Lamb ]
* Require at least Black version 23.1.0 to run the internal Black tests.
* Update copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

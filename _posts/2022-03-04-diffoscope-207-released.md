---
layout: post
title: diffoscope 207 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `207`. This version includes the following changes:

```
* Fix a gnarly regression when comparing directories against non-directories.
  (Closes: reproducible-builds/diffoscope#292)
* Use our assert_diff utility where we can within test_directory.py
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 254 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `254`. This version includes the following changes:

```
[ Chris Lamb ]
* Reflow some code according to black.

[ Seth Michael Larson ]
* Add support for comparing the 'eXtensible ARchive' (.XAR/.PKG) file format.

[ Vagrant Cascadian ]
* Add external tool on GNU Guix for 7z.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 217 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `217`. This version includes the following changes:

```
* Update test fixtures for GNU readelf 2.38 (now in Debian unstable).
* Be more specific about the minimum required version of readelf (ie.
  binutils) as it appears that this "patch" level version change resulted in
  a change of output, not the "minor" version. (Closes: #1013348)
* Don't leak the (likely-temporary) pathname when comparing PDF documents.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

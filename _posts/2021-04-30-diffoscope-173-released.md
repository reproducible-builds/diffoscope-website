---
layout: post
title: diffoscope 173 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `173`. This version includes the following changes:

```
[ Chris Lamb ]
* Add support for showing annotations in PDF files.
  (Closes: reproducible-builds/diffoscope#249)
* Move to assert_diff in test_pdf.py.

[ Zachary T Welch ]
* Difference.__init__: Demote unified_diff argument to a Python "kwarg".
```

You find out more by [visiting the project homepage](https://diffoscope.org).

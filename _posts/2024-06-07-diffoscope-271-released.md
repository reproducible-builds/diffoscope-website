---
layout: post
title: diffoscope 271 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `271`. This version includes the following changes:

```
[ Chris Lamb]
* Drop Build-Depends on liblz4-tool. Thanks, Chris Peterson.
  (Closes: #1072575)
* Update tests to support zipdetails version 4.004 shipped with Perl 5.40.
  (Closes: reproducible-builds/diffoscope#377)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 240 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `240`. This version includes the following changes:

```
[ Holger Levsen ]
* Update Lintian override info format in debian/source/lintian-overrides.
* Add Lintian overrides for some "very long lines" in test cases.
* Update Lintian overrides for tests being tagged source-is-missing or
  prebuilt.
* Add Lintian override for very long lines for debian/tests/control.
* Re-add two Lintian overrides about (well-known) source-is-missing
  instances.

[ Mattia Rizzolo ]
* Drop the use of include_package_data=True in setup.py.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 178 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `178`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't traceback on an broken symlink in a directory.
  (Closes: reproducible-builds/diffoscope#269)
* Rewrite the calculation of a file's "fuzzy hash" to make the control
  flow cleaner.

[ Balint Reczey ]
* Support .deb package members compressed with the Zstandard algorithm.
  (LP: #1923845)

[ Jean-Romain Garnier ]
* Overhaul the Mach-O executable file comparator.
* Implement tests for the Mach-O comparator.
* Switch to new argument format for the LLVM compiler.
* Fix test_libmix_differences in testsuite for the ELF format.
* Improve macOS compatibility for the Mach-O comparator.
* Add llvm-readobj and llvm-objdump to the internal EXTERNAL_TOOLS data
  structure.

[ Mattia Rizzolo ]
* Invoke gzip(1) with the short option variants to support Busybox's gzip.
  (Closes: reproducible-builds/diffoscope#268)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

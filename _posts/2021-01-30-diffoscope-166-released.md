---
layout: post
title: diffoscope 166 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `166`. This version includes the following changes:

```
[ Chris Lamb ]
* New features and bugfixes:
  - Explicitly remove our top-level temporary directory.
    (Closes: #981123, reproducible-builds/diffoscope#234)
  - Increase fuzzy matching threshold to 130 ensure that we show more
    differences. (Closes: reproducible-builds/diffoscope#232)
  - Save our sys.argv in our top-level temporary directory in case it
    helps debug current/errant temporary directories.
  - Prefer to use "magic.Magic" over the "magic.open" compatibility
    interface. (Closes: reproducible-builds/diffoscope#236)
  - Reduce fuzzy threshold to 110 to prevent some test failures.
    (Closes: reproducible-builds/diffoscope#233)

* Output improvements:
  - Show fuzzyness amount in percentage terms, not out of the
    rather-arbitrary "400".
  - Improve the logging of fuzzy matching.
  - Print the free space in our temporary directory when we create it, not
    from within diffoscope.main.

* Codebase improvements:
  - Tidy the diffoscopecomparators.utils.fuzzy module.
  - Update my copyright years.
  - Clarify the grammar of a comment.
  - Clarify in a comment that __del__ is not always called, so temporary
    directories are not neccessary removed the *moment* they go out of scope.

[ Conrad Ratschan ]
* Fix U-Boot Flattened Image Tree ("FIT") image detection for larger image
  files. (MR: reproducible-builds/diffoscope!75)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

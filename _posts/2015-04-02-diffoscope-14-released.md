---
layout: post
title: diffoscope 14 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `14`. This version includes the following changes:

```
* Remove diffutils from Recommends as it is Essential:yes.
* Fallback to Python hexlify if xxd is not available.
* Decode msgunfmt output using the actual PO charset.
* Stop buffering output of most external commands and
  send it to diff as it comes. (Closes: #781373)
* Stop feeding input to diff after a certain amount of lines,
  as GNU diff cannot cope with arbitrary large output.
* Fix newline and tab handling in HTML presenter.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 229 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `229`. This version includes the following changes:

```
[ Chris Lamb ]
* Skip test_html.py::test_diff if html2text is not installed.
  (Closes: #1026034)

[ Holger Levsen ]
* Bump standards version to 4.6.2, no changes needed.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

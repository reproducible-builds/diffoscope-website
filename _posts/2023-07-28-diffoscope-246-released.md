---
layout: post
title: diffoscope 246 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `246`. This version includes the following changes:

```
[ Gianfranco Costamagna ]
* Add support for LLVM 16.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

---
layout: post
title: diffoscope 245 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `245`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't include file size in image metadata; it is, at best, distracting and
  it is already in the directory metadata.
* Move to using assert_diff in ICO and JPEG tests.
* Update copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

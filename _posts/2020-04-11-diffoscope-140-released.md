---
layout: post
title: diffoscope 140 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `140`. This version includes the following changes:

```
* apktool 2.5.0 changed the handling of output of XML schemas, so update and
  restrict the corresponding test to match.
  (Closes: reproducible-builds/diffoscope#96)
* Add support for Hierarchical Data Format (HD5) files.
  (Closes: reproducible-builds/diffoscope#95)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

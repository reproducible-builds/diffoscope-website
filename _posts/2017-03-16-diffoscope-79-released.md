---
layout: post
title: diffoscope 79 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `79`. This version includes the following changes:

```
[ Chris Lamb ]
* Extract SquashFS images in one go rather than per-file, speeding up (eg.)
  Tails ISO comparison by ~10x.
* Support newer versions of cbfstool to avoid test failures.
  (Closes: #856446)
* Skip icc test that varies on endian if the Debian-specific patch is not
  present. (Closes: #856447)
* Compare GIF images using gifbuild. (Closes: #857610)
* Also interpret "DOS/MBR boot sector" files as ISO images as they may have
  been processed by isohybrid.
* Progress bar:
  - Hide bar if we are running with --debug mode.
  - Update prior to working on an item so the displayed filename is correct.

[ Maria Glukhova ]
* Improve AndroidManifest.xml comparison for APK files.
  - Indicate the AndroidManifest.xml type. (Closes: #850758)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

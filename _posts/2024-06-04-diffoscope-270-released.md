---
layout: post
title: diffoscope 270 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `270`. This version includes the following changes:

```
[ Chris Lamb ]
* No-change release due to broken version 269 tarballs.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

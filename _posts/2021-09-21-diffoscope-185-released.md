---
layout: post
title: diffoscope 185 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `185`. This version includes the following changes:

```
[ Mattia Rizzolo ]
* Fix the autopkgtest in order to fix testing migration: the androguard
  Python module is not in the python3-androguard Debian package
* Ignore a warning in the tests from the h5py package that doesn't concern
  diffoscope.

[ Chris Lamb ]
* Bump Standards-Version to 4.6.0.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

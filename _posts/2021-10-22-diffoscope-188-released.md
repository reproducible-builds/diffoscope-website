---
layout: post
title: diffoscope 188 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `188`. This version includes the following changes:

```
* Add support for Python Sphinx inventory files, usually named objects.inv.
* Fix Python bytecode decompilation tests with Python 3.10+.
  (Closes: reproducible-builds/diffoscope#278)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

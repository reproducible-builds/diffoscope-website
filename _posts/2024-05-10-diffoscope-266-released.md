---
layout: post
title: diffoscope 266 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `266`. This version includes the following changes:

```
[ Chris Lamb ]
* Use "xz --list" to supplement the output when comparing .xz archives;
  essential when some underlying metadata differs. (Closes: #1069329)
* Actually append the xz --list after the container differences, as it
  simplifies tests and the output.
* Add 7zip to <!nocheck> Build-Depends in debian/control.
* Update copyright years.

[ James Addison ]
* Maintain an in-header boolean state to determine whether to drop
  from-file/to-file lines. This fixes an issue where HTML differences were
  being inadvertendly neglected. (Closes: reproducible-builds/diffoscope#372)
```

You find out more by [visiting the project homepage](https://diffoscope.org).

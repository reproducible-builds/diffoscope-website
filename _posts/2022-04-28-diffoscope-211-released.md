---
layout: post
title: diffoscope 211 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `211`. This version includes the following changes:

```
[ Mattia Rizzolo ]
* Drop mplayer from the Build-Depends, it was add likely by accident and it's
  not needed.
* Disable gnumeric tests in Debian because it's not currently available.
```

You find out more by [visiting the project homepage](https://diffoscope.org).

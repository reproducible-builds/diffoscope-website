---
layout: post
title: diffoscope 247 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `247`. This version includes the following changes:

```
[ Chris Lamb ]
* Fix compataibility with file(1) version 5.45.
* Use assert_diff in test_uimage and test_cpio.

[ Roland Clobus ]
* xb-tool has moved in Debian bookworm.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
